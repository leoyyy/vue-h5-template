module.exports = function ({ types: babelTypes, template }) {
    return {
        name: "auto-console",
        visitor: {
            'FunctionDeclaration|ArrowFunctionExpression|FunctionExpression|ClassMethod'(path, state) {
                const coment = path.get("leadingComments")[0] || {};
                const comentNode = coment.node;

                if(!comentNode) {
                    path.findParent((parentPath) => {
                        const coment = parentPath.node.leadingComments;
                        if(!coment) {
                            // 当父级节点找不到时，返回false继续向上找
                            return false;
                        }else {
                          // 当找到leadingComments属性时，返回true结束查找
                          const comentNode = coment[0] || {};
                        //   console.log('yes find!', comentNode)
                          setConsole(path, state, template, comentNode)
                          return true;
                        }
                    })
            
                }else{
                    console.log('yes find')
                }
            }
            // Program(path, state){
            //   console.log('---', path)
            // }
        }
    };
};

function setConsole(path, state, template, comentNode){
    if(comentNode.type === 'CommentBlock'){
        const commentStr = comentNode.value.replace(/\s+/g, "");
        
        const commentArray = commentStr.split("*").filter(item => item)
        console.log('-------------', commentArray)
        const commentName = commentArray[0];
        if(commentName === '@tip'){
            importFunc(path, template)
        }
    }
}

function importFunc(path, template){
    path.get('body').node.body.unshift(template(`console.log('tips')`)());
}
