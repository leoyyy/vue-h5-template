const { merge } = require('webpack-merge');
// const CompressionPlugin = require('compression-webpack-plugin'); // gzip压缩
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin'); // 优化压缩css
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer'); // 打包分析
const common = require('./webpack.common');

const CompressAssetsPlugin = require('./plugins/CompressAssetsPlugin');

module.exports = merge(common, {
    mode: 'production',
    devtool: 'cheap-module-source-map',
    plugins: [
    // new BundleAnalyzerPlugin(),
        new CssMinimizerPlugin(),
        new CompressAssetsPlugin({
            output: 'result.zip',
        }),
        // new CompressionPlugin(),
    ],
    optimization: {
        splitChunks: {
            chunks: 'all',
            name: 'vendor',
            cacheGroups: {
                'echarts.vendor': {
                    name: 'echarts.vendor',
                    priority: 40,
                    test: /[\\/]node_modules[\\/](echarts|zrender)[\\/]/,
                    chunks: 'all',
                },
                lodash: {
                    name: 'lodash',
                    chunks: 'async',
                    test: /[\\/]node_modules[\\/]lodash[\\/]/,
                    priority: 40,
                },
                'async-common': {
                    chunks: 'async',
                    minChunks: 2,
                    name: 'async-commons',
                    priority: 30,
                },
                commons: {
                    name: 'commons',
                    chunks: 'all',
                    minChunks: 2,
                    priority: 20,
                },
            },
        },
    },
});
