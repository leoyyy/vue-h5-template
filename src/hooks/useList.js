import { ref } from "vue"

const useList = (listreq) => {
    if(!listreq){
        return new Error('请输入接口调用方法')
    }
    const list = ref([]);
    const getList = ()=> {
        listreq().then(res => {
            list.value = res
        })
    }
    return {
        list,
        getList
    }
}

export default useList