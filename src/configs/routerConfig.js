import UList from '../pages/ulist.vue';

const routes = [
    {
        path: '/ulist', component: UList,
    },
];

export default routes;
