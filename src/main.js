import { createApp } from 'vue';
import {
    createWebHistory,
    createRouter,
    // createWebHashHistory
} from 'vue-router';
import './assets/styles/index.scss';
// import $ from 'jquery'
import routes from './configs/routerConfig';

import App from './App.vue';

// const bad = "1";
// console.log('test=', good)
// console.log('process.env.NODE_ENV',process.env.NODE_ENV)

const router = createRouter({
    history: createWebHistory(),
    routes,
});

const app = createApp(App);

app.use(router);
app.mount('#app');

// const babel = require('babel-core');

// const result = babel.transform('const result = 1 + 2;', {
//     plugins: [
//         require('./testbabel')
//     ]
// })
