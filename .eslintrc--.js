module.exports = {
    env: {
        browser: true,
        es6: true,
        node: true,
    },
    extends: [
        'eslint:recommended',
        'plugin:vue/vue3-essential',
        'airbnb-base',
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
        // parser: 'vue-eslint-parser',
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    parser: 'vue-eslint-parser',
    plugins: [
        'vue',
    ],
    rules: {
        'global-require': 'off',
        'prefer-template': 0,
        indent: ['error', 4],
        'linebreak-style': ['off', 'windows'],
        'no-console': 'off',
        'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    },
};
